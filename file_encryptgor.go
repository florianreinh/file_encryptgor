package main

import (
	"flag"
	"file_encryptgor/encrypt"
	"file_encryptgor/decrypt"
)

/*
* The purpose of this file encryption tool is to have the possibility to asynchronously
* encrypt your files. Since RSA has a very small size limit for files, we use a workaround
* by encrypting the files with a randomly generated symmetric key suitable for file encryption,
* that then is encrypted using RSA. This allows you to store your encrypted key alongside the
* encrypted data, while you still own your RSA-key.
 */


 func main() {
	mode 		 			  	 :=  flag.String("mode", "dec", "Determines the application's task: Encryption or Decryption. Default: Decryption.")
	sym_key_name       :=  flag.String("sym-key-name", "symmetric_data_key", "Specifies the generated symmetric key's name.")
	file_src 					 :=  flag.String("file", "", "The path name of the file you want to encrypt (absolute or relative).")

	flag.Parse()

	if *mode == "enc" {
		// Use Encryptgor in Encryption Mode
		encrypt.WithAES256(*file_src, *sym_key_name)
	} else {
	  // Use Encryptgor in Decryption Mode (default)
		decrypt.WithAES256(*file_src, *sym_key_name)
	}
}
