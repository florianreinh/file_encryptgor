package decrypt

import (
  "file_encryptgor/utils"
  "file_encryptgor/files"
  "path/filepath"
  "io/ioutil"
  "github.com/gtank/cryptopasta"
  "strings"
)

func WithAES256(src string, sym_key_file_name string){
  srcDir := files.GetSrcDirectory(src)
  symmetricKey := srcDir + sym_key_file_name

  decryptFiles(symmetricKey, src)
}


/* Decrypt the target file using the decrypted symmetric key. Algorithm: AES 256 GCM */
func decryptFiles(symmetricKey string, src string) {
  srcType := files.GetSrcType(src)

  //read key file
  keyBytes, err := ioutil.ReadFile(symmetricKey)
  utils.LogAndQuitIfError("Could not read key: ", err)

  //take the first 32 bytes of the key and use it as a fixed size array for decryption (should be all bytes)
  var keyBytes32 [32]byte;
  copy(keyBytes32[:], keyBytes[:32])

  if srcType == files.TYPE_FILE {
    writeDecryptedFile(src, keyBytes32)

  } else if srcType == files.TYPE_DIRECTORY {
    target_files, err := filepath.Glob(files.GetSrcDirectory(src) + "*.enc")
    utils.LogAndQuitIfError("Could not glob " + src, err)

    for _, f := range target_files {
      writeDecryptedFile(f, keyBytes32)
    }
  } else {
    panic("Decryption failed: Could not determine src file type.")
  }
}

func writeDecryptedFile(src string, keyBytes32 [32]byte) {
  //read file to decode
  fileContent, err := ioutil.ReadFile(src)
  utils.LogAndQuitIfError("Could not read target file " + src, err)

  //decrypt target file using the generated key
  decryptedContent, err := cryptopasta.Decrypt(fileContent, &keyBytes32)
  utils.LogAndQuitIfError("Could not decrypt file " + src, err)

  //write the decrypted content to the original file
  decryptedFileName := strings.Replace(src, ".enc", "", -1)
  ioutil.WriteFile(decryptedFileName, decryptedContent, files.MODE_RW_R_R)
}

