package utils

import (
	"log"
)

func LogAndQuitIfError(prefix string, err error) {
	if err != nil {
		log.Fatal(prefix, err)
	}
}
