package crand

import (
  "crypto/rand"
  "encoding/base64"
)

func RandomBytes(n int) ([]byte, error){
  b := make([]byte, n)
  _, err := rand.Read(b)

  if err != nil { return nil, err }

  return b, nil
}

func RandomBytesBase64(n int) (string, error){
  s, err := RandomBytes(n)
  return base64.RawStdEncoding.EncodeToString(s), err
}
