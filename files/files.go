package files

import (
  "os"
  "path/filepath"
  "fmt"
  "file_encryptgor/utils"
)

const (
  TYPE_FILE      = "file"
  TYPE_DIRECTORY = "directory"

  MODE_RW_R_R = 0644
)

func GetSrcDirectory(src string) string {

  if stat, err := os.Stat(src); err == nil && stat.IsDir() {
    return src
  } else if err == nil && !stat.IsDir() {
    return filepath.Dir(src) + fmt.Sprintf("%c", filepath.Separator)
  } else {
    panic(src + " is not a file or directory.")
  }
}

func GetSrcType(src string) string {

  stat, err := os.Stat(src)
  utils.LogAndQuitIfError("Could not determine src type for " + src, err)

  file_mode := stat.Mode()

  switch {
  case file_mode.IsDir() : return TYPE_DIRECTORY
  case file_mode.IsRegular() : return TYPE_FILE
  default: panic("src is neither a directory nor a file.")
  }
}
