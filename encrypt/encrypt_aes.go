package encrypt

import (
  "os"
  "file_encryptgor/utils"
  "file_encryptgor/files"
  "path/filepath"
  "io/ioutil"
  "file_encryptgor/crand"
  "github.com/gtank/cryptopasta"
)

func WithAES256(src string, sym_key_file_name string){
  symmetric_key := getSymmetricKey(src, sym_key_file_name)
  encryptFiles(symmetric_key, src)
}


func getSymmetricKey(src string, sym_key_file_name string) string {
  symmetric_key := files.GetSrcDirectory(src) + sym_key_file_name

  /* If not existing, create symmetric key file based on random bits */
  if _, err := os.Stat(symmetric_key); os.IsNotExist(err) {
    bytes, err1 := crand.RandomBytes(256)
    utils.LogAndQuitIfError("Could not generate key content: ", err1)

    err := ioutil.WriteFile(symmetric_key, bytes, files.MODE_RW_R_R)
    utils.LogAndQuitIfError("Could not generate key: ", err)
  }

  return symmetric_key
}

/* Symmetrically encrypt the files to backup using the symmetric key. Encryption is done with the algorithm: AES 256 GCM. */
func encryptFiles(symmetricKey string, src string) {

  //read key file
  keyBytes, err := ioutil.ReadFile(symmetricKey)
  utils.LogAndQuitIfError("Could not read key: ", err)

  //take the first 32 bytes of the key and use it as a fixed size array for encryption (should be all bytes)
  var keyBytes32 [32]byte;
  copy(keyBytes32[:], keyBytes[:32])

  srcType := files.GetSrcType(src)

  if srcType == files.TYPE_FILE {

    writeEncryptedFile(src, keyBytes32)

  } else if srcType == files.TYPE_DIRECTORY {

    target_files, err := filepath.Glob(files.GetSrcDirectory(src) + "*")
    utils.LogAndQuitIfError("Could not glob "+src, err)

    for _, f := range target_files {
      if f != symmetricKey {
        writeEncryptedFile(f, keyBytes32)
      }
    }

  } else {
    panic("Encryption failed: Could not determine src file type.")
  }
}

func writeEncryptedFile(src string, keyBytes32 [32]byte) {
  //read target file
  fileContent, err := ioutil.ReadFile(src)
  utils.LogAndQuitIfError("Could not read target file "+src, err)

  //encrypt target file using the generated key
  encryptedContent, err := cryptopasta.Encrypt(fileContent, &keyBytes32)
  utils.LogAndQuitIfError("Could not encrypt file "+src, err)

  //write the encrypted content to the .enc file
  ioutil.WriteFile(src+".enc", encryptedContent, files.MODE_RW_R_R)
}
